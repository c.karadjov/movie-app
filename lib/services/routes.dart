import 'package:flutter/material.dart';
import 'package:movie_app/screens/authentication/login.dart';
import 'package:movie_app/screens/authentication/register.dart';
import 'package:movie_app/screens/main_screen.dart';
import 'package:movie_app/screens/movies/movie_details.dart';
import '../screens/intro.dart';

class AppRoutes {
  ///Auth routes
  static const String intro = 'intro';
  static const String login = 'login';
  static const String register = 'register';

  ///Main page with navigation
  static const String mainScreen = 'main-screen';

  ///Screens
  static const String movieDetails = 'movie-details';
}

class Routers {
  static Route<dynamic> generatedRoute(RouteSettings settings) {
    switch (settings.name) {

      ///Auth routes
      case AppRoutes.login:
        return MaterialPageRoute(
            settings: settings, builder: (context) => const Login());
      case AppRoutes.register:
        return MaterialPageRoute(
            settings: settings, builder: (context) => const Register());

      ///Main page with navigation
      case AppRoutes.mainScreen:
        return MaterialPageRoute(
            settings: settings, builder: (context) => const MainScreen());

      ///Screens
      case AppRoutes.movieDetails:
        return MaterialPageRoute(
            settings: settings,
            builder: (context) => MovieDetails(
                  params: settings.arguments,
                ));

      default:
        return MaterialPageRoute(
            settings: settings, builder: (context) => const Intro());
    }
  }
}
