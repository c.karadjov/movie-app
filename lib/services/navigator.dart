import 'package:flutter/material.dart';

class RootNavigationService {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName, {Object? arguments}) {
    return navigatorKey.currentState!
        .pushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> pushReplacementNamed(String routeName, {Object? arguments}) {
    return navigatorKey.currentState!
        .pushReplacementNamed(routeName, arguments: arguments);
  }

  Future<dynamic> goBack() async {
    return navigatorKey.currentState!.pop();
  }

  Future<dynamic> pushNamedAndRemoveUntil(
      String routeName, bool Function(Route<dynamic>) predicate,
      {Object? arguments}) async {
    navigatorKey.currentState!.pushNamedAndRemoveUntil(routeName, predicate);
  }

  Future<dynamic> popUntil(String routeName) async {
    navigatorKey.currentState!.popUntil(ModalRoute.withName(routeName));
  }
}

final RootNavigationService rootNavigationService = RootNavigationService();
