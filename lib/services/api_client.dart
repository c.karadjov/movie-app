import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ApiClient {
  final dio = createDio();
  final FlutterSecureStorage storage = const FlutterSecureStorage();
  final baseUrl =
      'https://raw.githubusercontent.com/FEND16/movie-json-data/master/json/';

  ApiClient._internal();

  static final _singleton = ApiClient._internal();

  factory ApiClient() => _singleton;

  static Dio createDio() {
    var dio = Dio(BaseOptions(headers: {
      'content-type': 'application/json',
      'Accept': 'application/json',
      'API-Key': ''
    }));

    dio.interceptors.addAll({
      AppInterceptors(dio),
    });
    return dio;
  }

  /// GET REQUEST
  Future<dynamic> get(String url, Map<String, dynamic>? queryParams) async {
    var response = await dio.get(
      baseUrl + url,
      queryParameters: queryParams,
    );
    return response.data;
  }

  /// POST REQUEST
  Future<dynamic> post(String url, Map<String, dynamic>? data) async {
    var response = await dio.post(
      baseUrl + url,
      data: data,
    );
    return response.data;
  }

  /// PUT REQUEST
  Future<dynamic> put(String url, Map<String, dynamic>? data) async {
    var response = await dio.put(
      baseUrl + url,
      data: data,
    );
    return response.data;
  }

  /// DELETE REQUEST
  Future<dynamic> delete(String url) async {
    var response = await dio.delete(
      baseUrl + url,
    );
    return response.data;
  }
}

/// Error handling
class AppInterceptors extends Interceptor {
  final Dio dio;

  AppInterceptors(this.dio);

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    switch (err.type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.receiveTimeout:
        throw DeadlineExceededException(err.requestOptions);
      case DioErrorType.response:
        switch (err.response?.statusCode) {
          case 400:
            throw BadRequestException(err.requestOptions, err.response);
          case 401:
            throw UnauthorizedException(err.requestOptions);
          case 402:
            throw BadRequestException(err.requestOptions, err.response);
          case 422:
            throw BadRequestException(err.requestOptions, err.response);
          case 403:
            throw BadRequestException(err.requestOptions, err.response);
          case 404:
            throw NotFoundException(err.requestOptions, err.response);
          case 405:
            throw NotAllowedException(err.requestOptions);
          case 409:
            throw ConflictException(err.requestOptions);
          case 500:
            throw InternalServerErrorException(err.requestOptions);
        }
        break;
      case DioErrorType.cancel:
        break;
      case DioErrorType.other:
        throw NoInternetConnectionException(err.requestOptions);
    }

    return handler.next(err);
  }
}

class BadRequestException extends DioError {
  BadRequestException(RequestOptions r, Response<dynamic>? response)
      : super(requestOptions: r, response: response);

  Map<String, dynamic> toMap() {
    return response!.data;
  }

  @override
  String toString() {
    return 'Invalid request';
  }
}

class InternalServerErrorException extends DioError {
  InternalServerErrorException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Unknown error occurred, please try again later.';
  }
}

class ConflictException extends DioError {
  ConflictException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Conflict occurred';
  }
}

class UnauthorizedException extends DioError {
  UnauthorizedException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Access denied';
  }
}

class NotFoundException extends DioError {
  NotFoundException(RequestOptions r, Response<dynamic>? response)
      : super(requestOptions: r, response: response);

  Map<String, dynamic> toMap() {
    return response!.data;
  }

  @override
  String toString() {
    return 'The requested information could not be found';
  }
}

class NotAllowedException extends DioError {
  NotAllowedException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Method not allowed';
  }
}

class NoInternetConnectionException extends DioError {
  NoInternetConnectionException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'No internet connection detected, please try again.';
  }
}

class DeadlineExceededException extends DioError {
  DeadlineExceededException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'The connection has timed out, please try again.';
  }
}
