import 'package:flutter/material.dart';
import 'package:movie_app/themes/color_theme.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField(
      {Key? key,
      this.controller,
      this.hintText,
      this.isObscured,
      this.textInputType,
      this.icon,
      this.contentPadding =
          const EdgeInsets.symmetric(vertical: 14.0, horizontal: 10.0),
      this.onChanged})
      : super(key: key);

  final TextEditingController? controller;
  final String? hintText;
  final bool? isObscured;
  final TextInputType? textInputType;
  final Widget? icon;
  final EdgeInsetsGeometry? contentPadding;
  final void Function(String)? onChanged;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        textAlignVertical: TextAlignVertical.center,
        style: const TextStyle(color: Colors.black, fontSize: 18),
        controller: controller,
        autocorrect: false,
        obscureText: isObscured!,
        keyboardType: textInputType,
        cursorColor: Colors.black,
        onChanged: onChanged,
        decoration: InputDecoration(
            prefixIcon: icon,
            errorStyle: TextStyle(color: Theme.of(context).primaryColor),
            filled: true,
            fillColor: Colors.white,
            contentPadding: contentPadding,
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5.0),
                borderSide:
                    const BorderSide(color: MyColorTheme.red, width: 1.0)),
            focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5.0),
                borderSide:
                    const BorderSide(color: MyColorTheme.red, width: 1.0)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5.0),
                borderSide:
                    const BorderSide(color: MyColorTheme.red, width: 1.0)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5.0),
                borderSide:
                    const BorderSide(color: MyColorTheme.red, width: 1.0)),
            hintText: hintText,
            hintStyle: const TextStyle(fontSize: 18.0, color: Colors.black)));
  }
}
