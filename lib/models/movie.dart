import 'dart:convert';

class Movie {
  final String? id;
  final String? title;
  final String? year;
  final List<String>? genres;
  final List<int>? ratings;
  final String? poster;
  final String? contentRating;
  final String? duration;
  final String? releaseDate;
  final int? averageRating;
  final String? originalTitle;
  final String? storyline;
  final List<String>? actors;
  final dynamic imdbRating;
  final String? posterurl;

  Movie({
    this.id,
    this.title,
    this.year,
    this.genres,
    this.ratings,
    this.poster,
    this.contentRating,
    this.duration,
    this.releaseDate,
    this.averageRating,
    this.originalTitle,
    this.storyline,
    this.actors,
    this.imdbRating,
    this.posterurl,
  });

  Movie copyWith({
    String? id,
    String? title,
    String? year,
    List<String>? genres,
    List<int>? ratings,
    String? poster,
    String? contentRating,
    String? duration,
    String? releaseDate,
    int? averageRating,
    String? originalTitle,
    String? storyline,
    List<String>? actors,
    dynamic imdbRating,
    String? posterurl,
  }) {
    return Movie(
      id: id ?? this.id,
      title: title ?? this.title,
      year: year ?? this.year,
      genres: genres ?? this.genres,
      ratings: ratings ?? this.ratings,
      poster: poster ?? this.poster,
      contentRating: contentRating ?? this.contentRating,
      duration: duration ?? this.duration,
      releaseDate: releaseDate ?? this.releaseDate,
      averageRating: averageRating ?? this.averageRating,
      originalTitle: originalTitle ?? this.originalTitle,
      storyline: storyline ?? this.storyline,
      actors: actors ?? this.actors,
      imdbRating: imdbRating ?? this.imdbRating,
      posterurl: posterurl ?? this.posterurl,
    );
  }

  Movie.fromJson(Map<String, dynamic> json)
      : id = json['id'] as String?,
        title = json['title'] as String?,
        year = json['year'] as String?,
        genres =
            (json['genres'] as List?)?.map((dynamic e) => e as String).toList(),
        ratings =
            (json['ratings'] as List?)?.map((dynamic e) => e as int).toList(),
        poster = json['poster'] as String?,
        contentRating = json['contentRating'] as String?,
        duration = json['duration'] as String?,
        releaseDate = json['releaseDate'] as String?,
        averageRating = json['averageRating'] as int?,
        originalTitle = json['originalTitle'] as String?,
        storyline = json['storyline'] as String?,
        actors =
            (json['actors'] as List?)?.map((dynamic e) => e as String).toList(),
        imdbRating = json['imdbRating'] as dynamic,
        posterurl = json['posterurl'] as String?;

  static Map<String, dynamic> toJson(Movie movie) => {
        'id': movie.id,
        'title': movie.title,
        'year': movie.year,
        'genres': movie.genres,
        'ratings': movie.ratings,
        'poster': movie.poster,
        'contentRating': movie.contentRating,
        'duration': movie.duration,
        'releaseDate': movie.releaseDate,
        'averageRating': movie.averageRating,
        'originalTitle': movie.originalTitle,
        'storyline': movie.storyline,
        'actors': movie.actors,
        'imdbRating': movie.imdbRating,
        'posterurl': movie.posterurl
      };

  static String encode(List<Movie> movies) => jsonEncode(
        movies
            .map<Map<String, dynamic>>((movie) => Movie.toJson(movie))
            .toList(),
      );

  static List<Movie> decode(String movies) =>
      (jsonDecode(movies) as List<dynamic>)
          .map<Movie>((item) => Movie.fromJson(item))
          .toList();
}
