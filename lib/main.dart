import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:movie_app/blocs/auth/auth_bloc.dart';
import 'package:movie_app/blocs/entry/entry_bloc.dart';
import 'package:movie_app/blocs/movie/movie_bloc.dart';
import 'package:movie_app/blocs/profile/profile_bloc.dart';
import 'package:movie_app/blocs/wach_list/wach_list_bloc.dart';
import 'package:movie_app/repositories/auth_repository.dart';
import 'package:movie_app/repositories/movie_repository.dart';
import 'package:movie_app/services/navigator.dart';
import 'package:movie_app/services/routes.dart';
import 'package:movie_app/themes/app_bar_theme.dart';
import 'package:movie_app/themes/button_theme.dart';
import 'package:movie_app/themes/color_theme.dart';
import 'package:movie_app/themes/input_theme.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  /// Disable rotate screen
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  /// Remove stored data if first run app
  final prefs = await SharedPreferences.getInstance();

  if (prefs.getBool('first_run') ?? true) {
    FlutterSecureStorage storage = const FlutterSecureStorage();

    await storage.deleteAll();

    prefs.setBool('first_run', false);
  }

  runApp(
    const MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => AuthRepository(),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => EntryBloc(AuthRepository())..add(StartApp()),
          ),
          BlocProvider(
            create: (context) => AuthBloc(AuthRepository()),
          ),
          BlocProvider(
            create: (context) => ProfileBloc(AuthRepository()),
          ),
          BlocProvider(
            create: (context) => MovieBloc(MovieRepository()),
          ),
          BlocProvider(
            create: (context) => WachListBloc(MovieRepository()),
          ),
        ],
        child: MaterialApp(
          title: '',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            // This is the theme of your application.
            textTheme: GoogleFonts.openSansTextTheme(
              Theme.of(context).textTheme,
            ).apply(
              bodyColor: MyColorTheme.black,
              displayColor: MyColorTheme.black,
            ),
            primarySwatch: MyColorTheme.primarySwatch,
            brightness: Brightness.light,
            primaryColor: MyColorTheme.primary,
            scaffoldBackgroundColor: MyColorTheme.white,
            elevatedButtonTheme: MyButtonTheme.elevatedButtonThemeData,
            inputDecorationTheme: MyInputDecoration.inputDecoration,
            appBarTheme: MyAppBarTheme.appBarTheme,
          ),
          onGenerateRoute: Routers.generatedRoute,
          navigatorKey: rootNavigationService.navigatorKey,
        ),
      ),
    );
  }
}
