import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:movie_app/themes/color_theme.dart';
import '../../themes/text_theme.dart';

class MessageService {
  static Future<ToastFuture> toast(context, String message, Color color) async {
    return showToast(message,
        context: context,
        animation: StyledToastAnimation.slideFromTopFade,
        reverseAnimation: StyledToastAnimation.slideToTopFade,
        position:
            const StyledToastPosition(align: Alignment.topCenter, offset: 5.0),
        duration: const Duration(seconds: 5),
        animDuration: const Duration(seconds: 1),
        curve: Curves.fastLinearToSlowEaseIn,
        reverseCurve: Curves.fastOutSlowIn,
        borderRadius: BorderRadius.circular(22),
        textAlign: TextAlign.center,
        backgroundColor: color,
        textStyle: MyTextTheme.medium.copyWith(color: MyColorTheme.white));
  }
}
