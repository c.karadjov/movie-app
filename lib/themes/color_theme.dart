import 'package:flutter/material.dart';

class MyColorTheme {
  static const Color primary = Color(0xFF1C1464);
  static const Color primaryDark = Color(0xFF1C1464);
  static const MaterialColor primarySwatch = Colors.indigo;

  static Color shadow = const Color(0xFF1243ac).withOpacity(0.5);

  static const Color gray = Color(0xFFF5F7FA);

  static const Color white = Color(0xFFFFFFFF);

  static const Color black = Colors.black;

  static const Color blackShade = Color.fromARGB(255, 108, 113, 122);

  static const Color green = Color(0xFF37D39B);

  static const Color red = Color(0xFFEC1E79);
}
