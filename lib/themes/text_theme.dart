import 'package:flutter/material.dart';
import 'package:movie_app/themes/color_theme.dart';

class MyTextTheme {
  static const TextStyle small = TextStyle(
    fontSize: 10.0,
    fontWeight: FontWeight.w400,
    color: MyColorTheme.black,
  );

  static const TextStyle regular = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: MyColorTheme.black,
  );

  static const TextStyle medium = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
    color: MyColorTheme.black,
  );

  static const TextStyle bold = TextStyle(
    fontSize: 25.0,
    fontWeight: FontWeight.w600,
    color: MyColorTheme.black,
  );

  static const TextStyle big = TextStyle(
    fontSize: 30.0,
    fontWeight: FontWeight.w700,
    color: MyColorTheme.black,
  );
}
