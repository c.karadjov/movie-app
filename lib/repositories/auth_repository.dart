import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthRepository {
  final FlutterSecureStorage storage = const FlutterSecureStorage();

  Future storeAccessToken(data) async {
    await storage.write(key: 'token', value: data);
  }

  Future<bool> hasToken() async {
    final token = await storage.read(key: 'token');
    return token != null;
  }

  Future deleteToken() async {
    await storage.delete(key: 'token');
  }
}
