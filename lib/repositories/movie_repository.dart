import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:movie_app/models/movie.dart';
import 'package:movie_app/services/api_client.dart';

class MovieRepository {
  final FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<Movie>> getMovies() async {
    final response = await ApiClient().get('movies-coming-soon.json', null);
    final data = json.decode(response);

    List<Movie>? movies = (data as List?)
        ?.map((dynamic e) => Movie.fromJson(e as Map<String, dynamic>))
        .toList();
    return movies!;
  }

  // Add new movie to wach list and store localy
  Future<void> addToWachList(Movie data, List<Movie> wachList) async {
    wachList.add(data);
    final String encodedData = Movie.encode(wachList);
    await storage.write(key: 'wachList', value: encodedData);
  }

  // Get wach list from storage
  Future<List<Movie>> getWachList() async {
    final data = await storage.read(key: 'wachList');
    final List<Movie> movies = data != null ? Movie.decode(data) : [];
    return movies;
  }

  // Store wach list localy
  Future<void> storeWachList(List<Movie> wachList) async {
    final String encodedData = Movie.encode(wachList);
    await storage.write(key: 'wachList', value: encodedData);
  }
}
