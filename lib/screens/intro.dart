import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/blocs/entry/entry_bloc.dart';
import 'package:movie_app/services/navigator.dart';
import 'package:movie_app/services/routes.dart';
import 'package:movie_app/themes/color_theme.dart';
import 'package:movie_app/themes/text_theme.dart';

class Intro extends StatefulWidget {
  const Intro({super.key});

  @override
  State<Intro> createState() => _IntroState();
}

class _IntroState extends State<Intro> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<EntryBloc, EntryState>(
          listener: (context, state) {
            if (state is AuthenticationAuthenticated) {
              rootNavigationService.pushNamedAndRemoveUntil(
                  AppRoutes.mainScreen, (route) => false);
            }
            if (state is AuthenticationUnauthenticated) {
              rootNavigationService.pushNamedAndRemoveUntil(
                  AppRoutes.login, (route) => false);
            }
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.center,
                child: Text('MOVIE APP',
                    style: MyTextTheme.big
                        .copyWith(color: MyColorTheme.red, fontSize: 40)),
              ),
              Align(
                alignment: Alignment.center,
                child: Image.asset(
                  'assets/images/edentechlabs.png',
                  width: 180,
                  fit: BoxFit.cover,
                ),
              ),
            ],
          )),
    );
  }
}
