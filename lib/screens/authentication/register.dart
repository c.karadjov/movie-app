import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/blocs/auth/auth_bloc.dart';
import 'package:movie_app/helpers/message_helper.dart';
import 'package:movie_app/services/navigator.dart';
import 'package:movie_app/services/routes.dart';
import 'package:movie_app/themes/color_theme.dart';
import 'package:movie_app/themes/text_theme.dart';
import 'package:movie_app/widgets/custom_text_field.dart';

class Register extends StatefulWidget {
  const Register({super.key});

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocConsumer<AuthBloc, AuthState>(
          listener: (context, state) {
            if (state is AuthFail) {
              MessageService.toast(context, state.error, MyColorTheme.red);
            }
            if (state is AuthSuccess) {
              String message = 'Sign in success!';
              MessageService.toast(context, message, MyColorTheme.green);
              rootNavigationService.navigateTo(AppRoutes.mainScreen);
            }
          },
          builder: (context, state) {
            return SingleChildScrollView(
              padding: const EdgeInsets.all(20),
              child: Center(
                child: Column(
                  children: [
                    const SizedBox(height: 40),
                    const Text(
                      'Sign up',
                      style: MyTextTheme.big,
                    ),
                    const SizedBox(height: 50),
                    CustomTextField(
                        controller: nameController,
                        hintText: 'Name',
                        isObscured: false,
                        textInputType: TextInputType.text,
                        icon: const Icon(
                          Icons.account_box_outlined,
                          color: MyColorTheme.primary,
                        )),
                    const SizedBox(height: 20),
                    CustomTextField(
                        controller: emailController,
                        hintText: 'Email',
                        isObscured: false,
                        textInputType: TextInputType.emailAddress,
                        icon: const Icon(
                          Icons.email_outlined,
                          color: MyColorTheme.primary,
                        )),
                    const SizedBox(height: 20),
                    CustomTextField(
                        controller: passwordController,
                        hintText: 'Password',
                        isObscured: true,
                        textInputType: TextInputType.text,
                        icon: const Icon(
                          Icons.lock_outline_sharp,
                          color: MyColorTheme.primary,
                        )),
                    const SizedBox(height: 20),
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () {
                          context.read<AuthBloc>().add(RegisterUser(
                              email: emailController.text,
                              password: passwordController.text,
                              name: nameController.text));
                        },
                        style: ElevatedButton.styleFrom(
                            padding: const EdgeInsets.symmetric(vertical: 12)),
                        child: const Text('Continue'),
                      ),
                    ),
                    const SizedBox(height: 20),
                    RichText(
                        text: TextSpan(children: [
                      const TextSpan(
                          text: 'Allready have an account. ',
                          style: MyTextTheme.medium),
                      TextSpan(
                          text: 'SIGN IN!',
                          recognizer: TapGestureRecognizer()
                            ..onTap = () => rootNavigationService
                                .navigateTo(AppRoutes.login),
                          style: MyTextTheme.medium.copyWith(
                              color: MyColorTheme.red,
                              decoration: TextDecoration.underline))
                    ])),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
