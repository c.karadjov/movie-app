import 'package:flutter/material.dart';
import 'package:movie_app/screens/movies/movies.dart';
import 'package:movie_app/screens/movies/watch_list.dart';
import 'package:movie_app/screens/profile/profile.dart';
import 'package:movie_app/themes/color_theme.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int currentIndex = 0;

  final List<Widget> buildScreens = [
    const Movies(),
    const WatchList(),
    const Profile(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildScreens[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.movie_filter_outlined, color: MyColorTheme.white),
            activeIcon:
                Icon(Icons.movie_filter_outlined, color: MyColorTheme.primary),
            label: 'Movies',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.play_circle_outline, color: MyColorTheme.white),
            activeIcon:
                Icon(Icons.play_circle_outline, color: MyColorTheme.primary),
            label: 'Watch list',
          ),
          BottomNavigationBarItem(
            icon:
                Icon(Icons.account_circle_outlined, color: MyColorTheme.white),
            activeIcon: Icon(Icons.account_circle_outlined,
                color: MyColorTheme.primary),
            label: 'Profile',
          ),
        ],
        currentIndex: currentIndex,
        elevation: 0,
        onTap: (value) {
          setState(() {
            currentIndex = value;
          });
        },
        type: BottomNavigationBarType.fixed,
        backgroundColor: MyColorTheme.red,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        selectedItemColor: MyColorTheme.primaryDark,
        unselectedItemColor: MyColorTheme.white,
      ),
    );
  }
}
