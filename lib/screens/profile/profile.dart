import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/blocs/profile/profile_bloc.dart';
import 'package:movie_app/helpers/message_helper.dart';
import 'package:movie_app/services/navigator.dart';
import 'package:movie_app/services/routes.dart';
import 'package:movie_app/themes/color_theme.dart';
import 'package:movie_app/themes/text_theme.dart';
import 'package:movie_app/widgets/custom_text_field.dart';
import 'package:movie_app/widgets/error_page.dart';
import 'package:movie_app/widgets/loading.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    context.read<ProfileBloc>().add(FetchProfile());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocConsumer<ProfileBloc, ProfileState>(
          listener: (context, state) {
            if (state is ProfileSuccess) {
              MessageService.toast(context, state.message, MyColorTheme.green);
              rootNavigationService.navigateTo(AppRoutes.login);
            }
            if (state is UpdateSuccess) {
              MessageService.toast(context, state.message, MyColorTheme.green);
            }
            if (state is ProfileUpdateFail) {
              MessageService.toast(context, state.error, MyColorTheme.red);
            }
          },
          builder: (context, state) {
            if (state is ProfileInitial) {
              return const Loading();
            }
            if (state is ProfileError) {
              return ErrorPage(
                  onRetryPressed: () {
                    context.read<ProfileBloc>().add(FetchProfile());
                  },
                  errorMessage: state.error);
            }
            if (state is ProfileFetched) {
              final user = state.user;
              nameController.text =
                  user.displayName == null ? 'App User' : user.displayName!;
              return SingleChildScrollView(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: MyColorTheme.red,
                      radius: 55,
                      child: CircleAvatar(
                        radius: 50,
                        backgroundColor: MyColorTheme.white,
                        child: Text(nameController.text[0],
                            style: MyTextTheme.big.copyWith(
                                fontSize: 58, color: MyColorTheme.primary)),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Text(user.email!,
                        style: MyTextTheme.bold.copyWith(
                            fontSize: 20, color: MyColorTheme.primary)),
                    const SizedBox(height: 20),
                    CustomTextField(
                        controller: nameController,
                        hintText: 'Name',
                        isObscured: false,
                        textInputType: TextInputType.text,
                        icon: const Icon(
                          Icons.account_box_outlined,
                          color: MyColorTheme.primary,
                        )),
                    const SizedBox(height: 20),
                    CustomTextField(
                        controller: passwordController,
                        hintText: 'Password',
                        isObscured: true,
                        textInputType: TextInputType.text,
                        icon: const Icon(
                          Icons.lock_outline_sharp,
                          color: MyColorTheme.primary,
                        )),
                    const SizedBox(height: 20),
                    SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 12)),
                            onPressed: () {
                              context.read<ProfileBloc>().add(UpdateUser(
                                  name: nameController.text,
                                  password: passwordController.text));
                            },
                            child: const Text('Update'))),
                    const SizedBox(height: 20),
                    SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: MyColorTheme.red,
                                padding:
                                    const EdgeInsets.symmetric(vertical: 12)),
                            onPressed: () {
                              context.read<ProfileBloc>().add(LogoutUser());
                            },
                            child: const Text('Sign out')))
                  ],
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
