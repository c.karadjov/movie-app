import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/blocs/movie/movie_bloc.dart';
import 'package:movie_app/models/movie.dart';
import 'package:movie_app/screens/movies/widgets/movie_card.dart';
import 'package:movie_app/services/navigator.dart';
import 'package:movie_app/services/routes.dart';
import 'package:movie_app/widgets/error_page.dart';
import 'package:movie_app/widgets/loading.dart';

class Movies extends StatefulWidget {
  const Movies({super.key});

  @override
  State<Movies> createState() => _MoviesState();
}

class _MoviesState extends State<Movies> {
  @override
  void initState() {
    super.initState();
    context.read<MovieBloc>().add(FetchMovies());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: BlocConsumer<MovieBloc, MovieState>(
        listener: (context, state) {
          // TODO: implement listener
        },
        builder: (context, state) {
          if (state is MovieInitial) {
            return const Loading();
          }
          if (state is MovieError) {
            return ErrorPage(
                errorMessage: state.error,
                onRetryPressed: () =>
                    context.read<MovieBloc>().add(FetchMovies()));
          }
          if (state is MovieFetched) {
            List<Movie> movies = state.movies;

            return GridView.builder(
                padding: const EdgeInsets.all(20),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 0.8,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10),
                itemCount: movies.length,
                itemBuilder: ((context, index) {
                  return GestureDetector(
                      onTap: () {
                        final params = {'movie': movies[index]};
                        rootNavigationService.navigateTo(AppRoutes.movieDetails,
                            arguments: params);
                      },
                      child: MovieCard(movie: movies[index]));
                }));
          }
          return Container();
        },
      )),
    );
  }
}
