import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/blocs/movie/movie_bloc.dart';
import 'package:movie_app/helpers/message_helper.dart';
import 'package:movie_app/models/movie.dart';
import 'package:movie_app/services/navigator.dart';
import 'package:movie_app/themes/color_theme.dart';
import 'package:movie_app/themes/text_theme.dart';

class MovieDetails extends StatefulWidget {
  const MovieDetails({super.key, required this.params});
  final dynamic params;

  @override
  State<MovieDetails> createState() => _MovieDetailsState();
}

class _MovieDetailsState extends State<MovieDetails> {
  @override
  Widget build(BuildContext context) {
    final Movie movie = widget.params['movie'];
    return Scaffold(
      appBar: AppBar(
        leading: Container(
          padding: const EdgeInsets.all(5),
          child: ElevatedButton(
            onPressed: () {
              rootNavigationService.goBack();
            },
            style: ElevatedButton.styleFrom(
              elevation: 0,
              shape: const CircleBorder(),
              backgroundColor: MyColorTheme.primary,
            ),
            child:
                const Icon(Icons.arrow_back_ios_new, color: MyColorTheme.white),
          ),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      extendBodyBehindAppBar: true,
      body: BlocConsumer<MovieBloc, MovieState>(
        listener: (context, state) {
          if (state is AddedToWachList) {
            MessageService.toast(context, state.message, MyColorTheme.green);
          }
        },
        builder: (context, state) {
          return SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height / 2,
                  child: Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(movie.posterurl!),
                              fit: BoxFit.cover))),
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                              child: Text(
                            movie.title!,
                            style: MyTextTheme.bold.copyWith(fontSize: 20),
                          )),
                          IconButton(
                              onPressed: () {
                                context
                                    .read<MovieBloc>()
                                    .add(AddToWachList(movie: movie));
                              },
                              icon: const Icon(
                                Icons.favorite_border,
                                size: 30,
                              ))
                        ],
                      ),
                      RichText(
                          text: TextSpan(children: [
                        TextSpan(
                            text: 'Released on ',
                            style: MyTextTheme.regular
                                .copyWith(color: MyColorTheme.blackShade)),
                        TextSpan(
                            text: movie.releaseDate,
                            style: MyTextTheme.regular
                                .copyWith(color: MyColorTheme.blackShade)),
                      ])),
                      Row(
                        children: [
                          for (var item in movie.genres!)
                            Text(item + ' ',
                                style: MyTextTheme.regular
                                    .copyWith(color: MyColorTheme.blackShade)),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Text(movie.storyline!),
                      const SizedBox(height: 10),
                      const Text('Starring: '),
                      Row(
                        children: [
                          for (var item in movie.actors!)
                            Text(item + ', ',
                                style: MyTextTheme.regular
                                    .copyWith(color: MyColorTheme.black)),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Align(
                        alignment: Alignment.centerRight,
                        child: RichText(
                            text: TextSpan(children: [
                          TextSpan(
                              text: 'Duration: ',
                              style: MyTextTheme.regular
                                  .copyWith(color: MyColorTheme.blackShade)),
                          TextSpan(
                              text: movie.duration,
                              style: MyTextTheme.regular
                                  .copyWith(color: MyColorTheme.blackShade)),
                        ])),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
