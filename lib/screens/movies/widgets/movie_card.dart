import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:movie_app/models/movie.dart';
import 'package:movie_app/themes/color_theme.dart';
import 'package:movie_app/themes/text_theme.dart';

class MovieCard extends StatelessWidget {
  const MovieCard(
      {super.key,
      required this.movie,
      this.onPressed,
      this.withRemoveButton = false});
  final Movie movie;
  final void Function()? onPressed;
  final bool withRemoveButton;

  @override
  Widget build(BuildContext context) {
    int sum = movie.ratings!.fold(0, (p, c) => p + c);
    double avarageRating = sum / movie.ratings!.length;
    return Column(
      children: [
        Expanded(
            // Some of image urls are broke.
            // The error is handle only on real mode.
            child: movie.posterurl!.isNotEmpty
                ? CachedNetworkImage(
                    imageUrl: movie.posterurl!,
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          alignment: Alignment.center,
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => const SpinKitFadingCircle(
                      color: MyColorTheme.black,
                      size: 50.0,
                    ),
                    errorWidget: (context, url, error) => const Icon(
                      Icons.image_not_supported_outlined,
                      size: 50,
                      color: MyColorTheme.primary,
                    ),
                  )
                : Container()),
        const SizedBox(height: 6),
        Column(
          children: [
            Text(
              movie.title!,
              style: MyTextTheme.regular.copyWith(fontWeight: FontWeight.bold),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
            const SizedBox(height: 3),
            Row(
              children: [
                Text(avarageRating.toStringAsFixed(1)),
                const SizedBox(width: 5),
                Expanded(
                  child: RatingBarIndicator(
                    rating: avarageRating.toDouble() / 2,
                    itemBuilder: (context, index) => const Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    itemCount: 5,
                    itemSize: 15.0,
                    direction: Axis.horizontal,
                  ),
                ),
                if (withRemoveButton)
                  InkWell(
                    onTap: onPressed,
                    child: const Icon(
                      Icons.delete_forever_sharp,
                      color: MyColorTheme.red,
                    ),
                  ),
              ],
            ),
          ],
        )
      ],
    );
  }
}
