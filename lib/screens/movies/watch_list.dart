import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/blocs/wach_list/wach_list_bloc.dart';
import 'package:movie_app/helpers/message_helper.dart';
import 'package:movie_app/models/movie.dart';
import 'package:movie_app/screens/movies/widgets/movie_card.dart';
import 'package:movie_app/themes/color_theme.dart';
import 'package:movie_app/themes/text_theme.dart';
import 'package:movie_app/widgets/error_page.dart';
import 'package:movie_app/widgets/loading.dart';

class WatchList extends StatefulWidget {
  const WatchList({super.key});

  @override
  State<WatchList> createState() => _WatchListState();
}

class _WatchListState extends State<WatchList> {
  @override
  void initState() {
    super.initState();
    context.read<WachListBloc>().add(FetchWachList());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: BlocConsumer<WachListBloc, WachListState>(
        listener: (context, state) {
          if (state is RemoveSuccess) {
            MessageService.toast(context, state.message, MyColorTheme.green);
          }
        },
        builder: (context, state) {
          if (state is WachListInitial) {
            return const Loading();
          }
          if (state is WachListError) {
            return ErrorPage(
                errorMessage: state.error,
                onRetryPressed: () =>
                    context.read<WachListBloc>().add(FetchWachList()));
          }
          if (state is WachListFetched) {
            List<Movie> movies = state.movies;
            return movies.isEmpty
                ? const Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Center(
                      child: Text(
                        'No added movie in wach list!',
                        style: MyTextTheme.bold,
                      ),
                    ),
                  )
                : GridView.builder(
                    padding: const EdgeInsets.all(20),
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 0.8,
                            crossAxisSpacing: 10,
                            mainAxisSpacing: 10),
                    itemCount: movies.length,
                    itemBuilder: ((context, index) {
                      return MovieCard(
                        movie: movies[index],
                        withRemoveButton: true,
                        onPressed: () {
                          context
                              .read<WachListBloc>()
                              .add(RemoveFromWachList(movie: movies[index]));
                        },
                      );
                    }));
          }
          return Container();
        },
      )),
    );
  }
}
