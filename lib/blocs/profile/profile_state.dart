part of 'profile_bloc.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();

  @override
  List<Object> get props => [];
}

class ProfileInitial extends ProfileState {}

class ProfileFetched extends ProfileState {
  final User user;
  const ProfileFetched({required this.user});

  @override
  List<Object> get props => [user];
}

class ProfileSuccess extends ProfileState {
  final String message;
  const ProfileSuccess({required this.message});
  @override
  List<Object> get props => [message];
}

class UpdateSuccess extends ProfileState {
  final String message;
  const UpdateSuccess({required this.message});
  @override
  List<Object> get props => [message];
}

class ProfileUpdateFail extends ProfileState {
  final dynamic error;
  const ProfileUpdateFail({required this.error});
  @override
  List<Object> get props => [identityHashCode(this)];
}

class ProfileError extends ProfileState {
  final dynamic error;
  const ProfileError({required this.error});
  @override
  List<Object> get props => [error];
}
