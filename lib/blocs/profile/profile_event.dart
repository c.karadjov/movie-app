part of 'profile_bloc.dart';

abstract class ProfileEvent extends Equatable {
  const ProfileEvent();

  @override
  List<Object> get props => [];
}

class FetchProfile extends ProfileEvent {
  @override
  List<Object> get props => [];
}

class UpdateUser extends ProfileEvent {
  final String name;
  final String password;
  const UpdateUser({required this.name, required this.password});
  @override
  List<Object> get props => [name, password];
}

class LogoutUser extends ProfileEvent {
  @override
  List<Object> get props => [];
}
