import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:movie_app/repositories/auth_repository.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final AuthRepository authRepository;
  ProfileBloc(this.authRepository) : super(ProfileInitial()) {
    on<FetchProfile>(onFetchProfile);
    on<LogoutUser>(onLogoutUser);
    on<UpdateUser>(onUpdateUser);
  }

  onFetchProfile(FetchProfile event, Emitter<ProfileState> emit) async {
    try {
      final User? user = FirebaseAuth.instance.currentUser;
      emit(ProfileFetched(user: user!));
    } catch (e) {
      emit(ProfileError(error: e.toString()));
    }
  }

  onUpdateUser(UpdateUser event, Emitter<ProfileState> emit) async {
    final state = this.state;
    if (state is ProfileFetched) {
      try {
        final User? user = FirebaseAuth.instance.currentUser;
        await user?.updateDisplayName(event.name);
        await user?.updatePassword(event.password);
        String message = 'Update user success!';
        emit(UpdateSuccess(message: message));
        emit(ProfileFetched(user: user!));
      } catch (e) {
        emit(ProfileUpdateFail(error: e.toString()));
        emit(ProfileFetched(user: state.user));
      }
    }
  }

  onLogoutUser(LogoutUser event, Emitter<ProfileState> emit) async {
    try {
      await FirebaseAuth.instance.signOut();
      await authRepository.deleteToken();
      String message = 'Sign out success!';
      emit(ProfileSuccess(message: message));
    } catch (e) {
      emit(ProfileError(error: e.toString()));
    }
  }
}
