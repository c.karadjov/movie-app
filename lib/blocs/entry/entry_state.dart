part of 'entry_bloc.dart';

abstract class EntryState extends Equatable {
  const EntryState();

  @override
  List<Object> get props => [];
}

class EntryInitial extends EntryState {}

class AuthenticationAuthenticated extends EntryState {
  @override
  List<Object> get props => [];
}

class AuthenticationUnauthenticated extends EntryState {
  @override
  List<Object> get props => [];
}

class AuthenticationError extends EntryState {
  final String error;
  const AuthenticationError(this.error);

  @override
  List<Object> get props => [error];
}
