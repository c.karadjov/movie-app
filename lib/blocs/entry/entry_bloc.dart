import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/repositories/auth_repository.dart';

part 'entry_event.dart';
part 'entry_state.dart';

class EntryBloc extends Bloc<EntryEvent, EntryState> {
  final AuthRepository authRepository;
  EntryBloc(this.authRepository) : super(EntryInitial()) {
    on<StartApp>(onStartApp);
    on<AppStarted>(onAppStarted);
  }

  onStartApp(event, emit) async {
    Future.delayed(const Duration(seconds: 3), () {
      add(AppStarted());
    });
  }

  onAppStarted(event, emit) async {
    try {
      if (await authRepository.hasToken()) {
        emit(AuthenticationAuthenticated());
      } else {
        emit(AuthenticationUnauthenticated());
      }
    } catch (e) {
      emit(AuthenticationError(e.toString()));
    }
  }
}
