part of 'entry_bloc.dart';

abstract class EntryEvent extends Equatable {
  const EntryEvent();

  @override
  List<Object> get props => [];
}

class StartApp extends EntryEvent {
  @override
  List<Object> get props => [];
}

class AppStarted extends EntryEvent {
  @override
  List<Object> get props => [];
}

class UserAuthenticate extends EntryEvent {
  @override
  List<Object> get props => [];
}

class UserUnauthenticate extends EntryEvent {
  @override
  List<Object> get props => [];
}
