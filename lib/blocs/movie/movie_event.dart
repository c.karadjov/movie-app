part of 'movie_bloc.dart';

abstract class MovieEvent extends Equatable {
  const MovieEvent();

  @override
  List<Object> get props => [];
}

class FetchMovies extends MovieEvent {
  @override
  List<Object> get props => [];
}

class AddToWachList extends MovieEvent {
  final Movie movie;
  const AddToWachList({required this.movie});
  @override
  List<Object> get props => [movie];
}

class RemoveFromWatchList extends MovieEvent {
  @override
  List<Object> get props => [];
}
