part of 'movie_bloc.dart';

abstract class MovieState extends Equatable {
  const MovieState();

  @override
  List<Object> get props => [];
}

class MovieInitial extends MovieState {}

class MovieFetched extends MovieState {
  final List<Movie> movies;
  const MovieFetched({required this.movies});

  @override
  List<Object> get props => [movies];
}

class MovieError extends MovieState {
  final String error;
  const MovieError({required this.error});

  @override
  List<Object> get props => [error];
}

class AddedToWachList extends MovieState {
  final String message;
  const AddedToWachList({required this.message});
  @override
  List<Object> get props => [identityHashCode(this)];
}
