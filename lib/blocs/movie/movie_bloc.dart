import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/models/movie.dart';
import 'package:movie_app/repositories/movie_repository.dart';

part 'movie_event.dart';
part 'movie_state.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  final MovieRepository movieRepository;
  MovieBloc(this.movieRepository) : super(MovieInitial()) {
    on<FetchMovies>(onFetchMovies);
    on<AddToWachList>(onAddToWachList);
  }

  onFetchMovies(FetchMovies event, Emitter<MovieState> emit) async {
    try {
      final List<Movie> movies = await movieRepository.getMovies();
      emit(MovieFetched(movies: movies));
    } catch (e) {
      emit(MovieError(error: e.toString()));
    }
  }

  onAddToWachList(AddToWachList event, Emitter<MovieState> emit) async {
    final state = this.state;
    if (state is MovieFetched) {
      try {
        final data = event.movie;
        final wachList = await movieRepository.getWachList();
        // Check for existing movie in current wach list
        var exist = wachList.firstWhereOrNull((el) => el.id == data.id);
        if (exist == null) {
          await movieRepository.addToWachList(data, wachList);
          String message = "The movie was successful added to wach list!";
          emit(AddedToWachList(message: message));
          emit(MovieFetched(movies: state.movies));
        } else {
          String message = "The movie has allready added to wach list!";
          emit(AddedToWachList(message: message));
          emit(MovieFetched(movies: state.movies));
        }
      } catch (e) {
        emit(MovieError(error: e.toString()));
      }
    }
  }
}
