part of 'wach_list_bloc.dart';

abstract class WachListEvent extends Equatable {
  const WachListEvent();

  @override
  List<Object> get props => [];
}

class FetchWachList extends WachListEvent {
  @override
  List<Object> get props => [];
}

class RemoveFromWachList extends WachListEvent {
  final Movie movie;
  const RemoveFromWachList({required this.movie});
  @override
  List<Object> get props => [movie];
}
