import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/models/movie.dart';
import 'package:movie_app/repositories/movie_repository.dart';

part 'wach_list_event.dart';
part 'wach_list_state.dart';

class WachListBloc extends Bloc<WachListEvent, WachListState> {
  final MovieRepository movieRepository;
  WachListBloc(this.movieRepository) : super(WachListInitial()) {
    on<FetchWachList>(onFetchWachList);
    on<RemoveFromWachList>(onRemoveFromWachList);
  }

  onFetchWachList(FetchWachList event, Emitter<WachListState> emit) async {
    try {
      final List<Movie> movies = await movieRepository.getWachList();
      emit(WachListFetched(movies: movies));
    } catch (e) {
      emit(WachListError(error: e.toString()));
    }
  }

  onRemoveFromWachList(
      RemoveFromWachList event, Emitter<WachListState> emit) async {
    final state = this.state;
    if (state is WachListFetched) {
      try {
        state.movies.removeWhere((element) => element.id == event.movie.id);
        await movieRepository.storeWachList(state.movies);
        String message = 'Movie has been successful removed from wach list!';
        emit(RemoveSuccess(message: message));
        emit(WachListFetched(movies: state.movies));
      } catch (e) {
        emit(WachListError(error: e.toString()));
      }
    }
  }
}
