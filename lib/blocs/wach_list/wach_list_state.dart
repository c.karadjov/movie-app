part of 'wach_list_bloc.dart';

abstract class WachListState extends Equatable {
  const WachListState();

  @override
  List<Object> get props => [];
}

class WachListInitial extends WachListState {}

class WachListFetched extends WachListState {
  final List<Movie> movies;
  const WachListFetched({required this.movies});

  @override
  List<Object> get props => [movies];
}

class WachListError extends WachListState {
  final String error;
  const WachListError({required this.error});

  @override
  List<Object> get props => [error];
}

class RemoveSuccess extends WachListState {
  final String message;
  const RemoveSuccess({required this.message});

  @override
  List<Object> get props => [message];
}
