import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/repositories/auth_repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository authRepository;
  AuthBloc(this.authRepository) : super(AuthInitial()) {
    on<RegisterUser>(onRegisterUser);
    on<LoginUser>(onLoginUser);
  }

  onRegisterUser(RegisterUser event, Emitter<AuthState> emit) async {
    try {
      final credential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: event.email, password: event.password);
      String token = credential.user!.uid;
      await authRepository.storeAccessToken(token);
      emit(AuthSuccess());
    } catch (e) {
      emit(AuthFail(error: e.toString()));
    }
  }

  onLoginUser(LoginUser event, Emitter<AuthState> emit) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(
              email: event.email, password: event.password);
      String token = userCredential.user!.uid;
      await authRepository.storeAccessToken(token);
      emit(AuthSuccess());
    } catch (e) {
      emit(AuthFail(error: e.toString()));
    }
  }
}
