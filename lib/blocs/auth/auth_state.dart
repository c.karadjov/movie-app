part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class AuthSuccess extends AuthState {
  @override
  List<Object> get props => [];
}

class AuthFail extends AuthState {
  final dynamic error;
  const AuthFail({required this.error});
  @override
  List<Object> get props => [identityHashCode(this)];
}
